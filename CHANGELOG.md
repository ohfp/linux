# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

This project follows the official Firefox releases, but also uses
[Arch Linux Package Versioning](https://wiki.archlinux.org/index.php/Arch_package_guidelines#Package_versioning) to mark individual releases in between versions.

## [Unreleased]

## [74.0-2] - 2020-03-10

### Fixed

- Wrong commenting format in `librewolf.cfg` breaking LibreWolf settings

### Known Issues

- Appimage and FlatPak releases must be considered broken

## [74.0-1] - 2020-03-10

### Added

- Initial release following stable upstream Firefox releases
- [WIP] Appimage and FlatPak Releases
